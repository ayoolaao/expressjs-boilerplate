import startServer from './server';
import chalk from 'chalk';

const { VERSION_SHA = '0.0.1' } = process.env;

startServer()
  .then(() => {
    console.info(`${chalk.blue(`[Time: ${Date.now()}]`)} Express API Server started successfully, Software Version SHA: ${chalk.yellow(VERSION_SHA)} \n`);
  })
  .catch(error => {
  Error.captureStackTrace(error);
  console.error(`${chalk.blue(`[Time: ${Date.now()}]`)} [ERROR] ${error} [STACK] ${error.stack}`);
})

