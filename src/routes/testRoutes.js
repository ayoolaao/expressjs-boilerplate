import express from 'express';
import TestController from '../controllers/testController';

const router = express.Router();
const testController = new TestController();

router.get('/', testController.retrieveAllTests);
router.get('/:testId', testController.retrieveTest);

export default router;
