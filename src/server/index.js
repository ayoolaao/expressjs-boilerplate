import express from 'express';
import cors from 'cors';
import chalk from 'chalk';

import testRoutes from '../routes/testRoutes';

const { PORT = 3015 } = process.env;

const startServer = async () => {
  const app = express();

  app.use('*', cors({
    origin: ['http://localhost:3000', /https:\/\/.*\.ayoolaao\.io/],
    credentials: false, // TODO Set to true once auth is in place
  }));

  app.use(express.urlencoded({ extended: true }));
  app.use(express.json({ limit: '50mb' }));

  app.use((req, res, next) => {
    console.info(`${chalk.blue(`[Time: ${Date.now()}]`)} ${chalk.greenBright(req.method)} - ${chalk.yellow(req.originalUrl)}`); // eslint-disable-line no-console
    next();
  });

  // Public Routes
  app.route('/').all((req, res) => {
    res.json({
      name: 'Express API BoilerPlate',
      status: 'API Dev in progress',
    });
  });

  // Protected Routes
  // Add OKTA or KeyCloak etc protected routes in here

  // User Routes
  // Import from .routes/ dir
  app.use('/test', testRoutes);

  app.listen(PORT, () => {
    console.info(`${chalk.blue(`[Time: ${Date.now()}]`)} Express Server is now running on ${chalk.yellow(`http://localhost:${PORT}`)}`); // eslint-disable-line no-console
  });
};

export default startServer;
