const generateRandomNumber = (limit) => Math.floor(Math.random() * limit);

export default generateRandomNumber;
