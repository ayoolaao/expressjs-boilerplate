import { findAllTests, findTestById } from '../models/testModel';

export default class TestController {
  retrieveAllTests = async (req, res) => {
    try {
      const allTests = await findAllTests();
      res.send(allTests);
    } catch (error) {
      res.sendStatus(400);
    }
  }

  retrieveTest = async (req, res) => {
    const { testId } = req.params;

    try {
      const testDetails = await findTestById(testId);
      res.send(testDetails);
    } catch (error) {
      res.sendStatus(404);
    }
  }
}
