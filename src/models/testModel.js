const testsMockData = [
  { id: 0, name: 'test 0' },
  { id: 1, name: 'test 1' },
];

export const findAllTests = async () => testsMockData;

export const findTestById = async (testId) => {
  const testData = testsMockData.find((test) => test.id === Number(testId));
  return testData || throw new Error('NOT FOUND');
};
